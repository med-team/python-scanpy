#!/usr/bin/make -f
# output every command that modifies files on the build system.
export DH_VERBOSE = 1

export PYBUILD_NAME=scanpy

export PYBUILD_TEST_ARGS=-v
#export PYBUILD_BEFORE_TEST=cp -av pyproject.toml {build_dir}; cp -a $(PYBUILD_NAME)/tests {build_dir}/$(PYBUILD_NAME)
#export PYBUILD_AFTER_TEST=rm -rf {build_dir}/pyproject.toml {build_dir}/$(PYBUILD_NAME)/tests

%:
	dh $@ --buildsystem=pybuild

override_dh_auto_clean:
#	dh_auto_clean
	${RM} -rf .pybuild __pycache__ scanpy/tests/__pycache__ scanpy/__pycache__

# Needs louvain package (vtaag?)
export EXCLUDED_TESTS=test_cluster_subset or test_louvain_basic or test_partition_type or test_neighbors_key_obsp or test_rank_genes_groups_logreg or test_paga_paul15_subsampled or test_clustering_subset or test_pbmc3k
# ran out of memory
EXCLUDED_TESTS += or test_seurat_v3_mean_var_output_with_batchkey or test_pca_reproducible or test_higly_variable_genes_compare_to_seurat_v3
# external tests
EXCLUDED_TESTS += or test_harmony_timeseries or test_scanorama or test_scrublet
# Broken test https://github.com/theislab/scanpy/issues/2048
EXCLUDED_TESTS += or test_visium_default

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	cp -a debian/data data
	cp -ab debian/_images/*.png scanpy/tests/_images/
	PYBUILD_SYSTEM=custom \
	PYBUILD_TEST_ARGS="pytest-3 -rx -v -k \"not ( $(EXCLUDED_TESTS) ) \" --ignore=scanpy/tests/_images -cov=scanpy --cov=tests --cov=docs_src --cov-report=term-missing:skip-covered -o console_output_style=progress" \
	dh_auto_test
	# restore upstream images
	for f in scanpy/tests/_images/*~ ; do mv -v $$f $${f%?} ; done
endif

# If you need to rebuild the Sphinx documentation
# Add sphinxdoc to the dh --with line
#
# And uncomment the following lines
#override_dh_auto_build: export http_proxy=127.0.0.1:9
#override_dh_auto_build: export https_proxy=127.0.0.1:9
#override_dh_auto_build:
#	dh_auto_build
#	PYTHONPATH=. python3 -m sphinx -N -bhtml \
#	docs/ build/html # HTML generator
#	PYTHONPATH=. python3 -m sphinx -N -bman \
#	docs/ build/man # Manpage generator
