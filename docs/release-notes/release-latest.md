## Version 1.9

```{include} /release-notes/1.9.6.md
```

```{include} /release-notes/1.9.5.md
```

```{include} /release-notes/1.9.4.md
```

```{include} /release-notes/1.9.3.md
```

```{include} /release-notes/1.9.2.md
```

```{include} /release-notes/1.9.1.md
```

```{include} /release-notes/1.9.0.md
```
